<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BossEmployee extends Model
{
    protected $table = 'boss_employee';

    public function user() {
        return $this->hasOne('App\User', 'id', 'employee_id');
    }
}
