<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CE extends Model
{
    protected $name;
    protected $address;
    protected $opening_hours;

    protected $fillable = array('name', 'address','opening_hours');
    protected $table = 'ce';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('id', $id)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información del centro de emision';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el centro de emision.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try {
            $response->rows = self::where('active', 1)->orderBy('name', 'asc')->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de centros de emision';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las centros de emision.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createObject(CE $object)
    {
        $response = new Response();
        try {
            $object->save();
            $response->code = 201;
            $response->msg = 'Centro de emision creado correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear el centro de emision';
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

    public static function updateObject($id, $data)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Categoría modificada exitosamente';
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al modificar el centro de emision';
            $response->exception = $e->getMessage().''.$e->getLine();
        }

        return $response;
    }

    public static function deleteObject($id)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            if ($object) {
                $object->active = 0;
                $object->save();
                $response->msg = 'Centro de emision eliminado correctamente';
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = 'No se encontro informacion.';
            }

            $response->code = 200;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al eliminar el centro de emision';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
