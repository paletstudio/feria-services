<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $name;
    protected $description;
    protected $category_type_id;

    protected $fillable = array('name', 'description');
    protected $table = 'category';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function categoryvalues(){
        return $this->hasMany('App\CategoryValue');
    }

    public function subcategory(){
        return $this->hasMany('App\Subcategory');
    }

    public function type()
    {
        return $this->belongsTo('App\CategoryType', 'category_type_id');
    }

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('id', $id)->where('active', 1)->with('type')->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de categorías';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener la categoría.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try {
            $info = request('info', false);
            $type = request('type', null);

            $rows = self::where('active', 1)->with('type');

            if ($type) $rows->where('category_type_id', $type);
            if ($info) $rows->with('subcategory.data', 'subcategory.issues.data');
            $rows = $rows->get();

            if ($info) {
                //logica para subir niveles la estructura subcategory->issue->data
                foreach($rows as $row) {
                    foreach ($row->subcategory as $subcategory) {
                        foreach ($subcategory->issues as $subcategory_issue) {
                            $temp = $subcategory_issue->data;
                            unset($subcategory_issue->data);
                            $subcategory_issue->data = $temp->map(function ($item) {
                                return $item->data;
                            });
                        }

                        $temp = $subcategory->issues;
                        unset($subcategory->issues);
                        $subcategory->issues = $temp->map(function($item) {
                            $item->issue->data = $item->data;
                            unset($item->data);
                            return $item->issue;
                        });
                    }
                }
            }

            $response->rows = $rows;
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de categorías';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las categorías.';
            $response->exception = $e->getMessage().' '.$e->getLine();
        }

        return $response;
    }

    public static function getAllByRoute()
    {
        $response = new Response();

        try {
            $rows = self::where('use_routes', 1)->get();
            $response->rows = $rows;
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de categorías';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las categorías.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createObject(Category $object)
    {
        $response = new Response();
        try {
            $object->save();
            $response->code = 201;
            $response->msg = 'Categoría creada correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear la categoría';
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

    public static function updateObject($id, $data)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Categoría modificada exitosamente';
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al modificar la categoría';
            $response->exception = $e->getMessage().''.$e->getLine();
        }

        return $response;
    }

    public static function deleteObject($id)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            if ($object) {
                $object->active = 0;
                $object->save();
                $response->msg = 'Categoría borrada correctamente';
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = 'No se encontro informacion.';
            }

            $response->code = 200;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al borrar la categoría';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
