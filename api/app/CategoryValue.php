<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryValue extends Model
{
    protected $table = 'category_value';
    protected $hidden = ['id', 'category_id'];
}
