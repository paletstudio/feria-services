<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = 'data';

    public function data_type()
    {
        return $this->belongsTo('App\DataType');
    }
}
