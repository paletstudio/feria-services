<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estacion extends Model
{
    protected $name;
    protected $description;

    protected $fillable = array('category_id', 'name', 'description');
    protected $table = 'estacion';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('id', $id)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de la estacion.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el registro de la estacion.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try {
            $response->rows = self::where('active', 1)->orderBy('name', 'asc')->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de estaciones';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las estaciones.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createObject(Estacion $object)
    {
        $response = new Response();
        try {
            $object->save();
            $response->code = 201;
            $response->msg = 'Estacion creada correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear la estacion';
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

    public static function updateObject($id, $data)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Estacion modificada exitosamente';
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al modificar la estacion';
            $response->exception = $e->getMessage().''.$e->getLine();
        }

        return $response;
    }

    public static function deleteObject($id)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            if ($object) {
                $object->active = 0;
                $object->save();
                $response->msg = 'Estacion eliminada correctamente';
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = 'No se encontro informacion con este id.';
            }

            $response->code = 200;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al eliminar la estacion';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
