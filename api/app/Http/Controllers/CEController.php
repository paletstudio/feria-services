<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class CEController extends Controller
{
    public function index()
    {
        $response = \App\CE::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = (object)Request::all();
        $object = Utils::cast('\App\CE', $object);
        $response = \App\CE::createObject($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\CE::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\CE::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\CE::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }
}
