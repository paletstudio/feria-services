<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class CategoryController extends Controller
{

    public function index()
    {
        $info = false;
        $response = \App\Category::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = (object)Request::all();
        $object = Utils::cast('\App\Category', $object);
        $response = \App\Category::createObject($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Category::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\Category::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Category::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function getAllByRoute()
    {
        $response = \App\Category::getAllByRoute();
        return response()->json($response)->setStatusCode($response->code);
    }
}
