<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class EstacionController extends Controller
{
    public function index()
    {
        $response = \App\Estacion::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = (object)Request::all();
        $object = Utils::cast('\App\Estacion', $object);
        $response = \App\Estacion::createObject($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Estacion::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\Estacion::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Estacion::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }
}
