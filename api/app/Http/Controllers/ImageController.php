<?php

namespace App\Http\Controllers;

use Response;

class ImageController extends Controller
{
    public function display($uniqueid)
    {
        $image = \App\RequestImage::where('uniqueid', '=', $uniqueid)->firstOrFail();
        $pathToFile = public_path($image->image_url);

        return response()->file($pathToFile);
    }
}
