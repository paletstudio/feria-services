<?php

namespace App\Http\Controllers;

use Request;
use Response;
use JWTAuth;

class IndicatorController extends Controller
{
    public function requestStatus()
    {
        $user_id = 0;
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();
            $role = $payload['role'];
            if($role === 'auditor'){
                $user_id = $payload['user']['id'];
            }
        }

        $response = \App\Indicator::requestStatus($user_id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function category()
    {
        $user_id = 0;
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();
            $role = $payload['role'];
            if($role === 'auditor'){
                $user_id = $payload['user']['id'];
            }
        }

        $response = \App\Indicator::category($user_id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function categoryAverage()
    {
        $user_id = 0;
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();
            $role = $payload['role'];
            if($role === 'auditor'){
                $user_id = $payload['user']['id'];
            }
        }

        $response = \App\Indicator::categoryAverage($user_id);

        return response()->json($response)->setStatusCode($response->code);
    }
}
