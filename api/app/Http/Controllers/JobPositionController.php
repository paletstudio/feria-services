<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class JobPositionController extends Controller
{

    public function index()
    {
        $response = \App\Jobposition::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $category = (object)Request::all();
        $category = Utils::cast('\App\Jobposition', $category);
        $response = \App\Jobposition::createObject($category);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Jobposition::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $category = Request::all();
        $response = \App\Jobposition::updateObject($id, $category);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Jobposition::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }

}
