<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class OxxoController extends Controller
{
    public function index()
    {
        $response = \App\Oxxo::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = (object)Request::all();
        $object = Utils::cast('\App\Oxxo', $object);
        $response = \App\Oxxo::createObject($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Oxxo::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\Oxxo::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Oxxo::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }
}
