<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProfileController extends Controller
{

    public function index()
    {
        $response = \App\Profile::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function show($id)
    {
        $response = \App\Profile::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

}
