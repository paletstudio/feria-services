<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Excel;
use JWTAuth;

class ReportController extends Controller
{
    public function request()
    {
        $auditor_id = -1;
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();
            $role = $payload['role'];
            if($role === 'auditor'){
                $auditor_id = $payload['user']['id'];
            }
        }

        // $query = Request::query();
        $response = \App\Report::request($auditor_id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function requestToExcel()
    {
        $auditor_id = -1;
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();
            $role = $payload['role'];
            if($role === 'auditor'){
                $auditor_id = $payload['user']['id'];
            }
        }

        $data = \App\Report::request($auditor_id, $paginate = false)->rows;
        \Log::debug('excel', array('etsel', $data));
        $date = date('Y-m-d H:i:s');
        $name = 'Request_Report_'.$date;

        Excel::create($name, function ($excel) use ($data) {
                $excel->setTitle('Request report');

                $excel->sheet('Report', function ($sheet) use ($data) {

                    foreach ($data as $index => $row) {
                        $excelHeader = [];
                        $excelRow = [];
                        $row = \App\Utils::filterExcelValues($row);

                        foreach ($row as $key => $value) {
                            array_push($excelRow, $value);
                            if ($index === 0) {
                                array_push($excelHeader, $key);
                            }
                        }

                        if ($index === 0) {
                            $sheet->row(1, $excelHeader);
                        }
                        $sheet->appendRow($excelRow);
                    }
                });

            })->download('xlsx');

    }
}
