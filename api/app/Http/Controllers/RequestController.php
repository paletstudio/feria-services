<?php

namespace App\Http\Controllers;

use Request;
use Response;
use JWTAuth;
use Event;
use \App\Events\NewRequest;

class RequestController extends Controller
{
    public function index()
    {
        $response = \App\Request::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function getCountByStatus(){
        $status = 1;
        if(null !== Request::input('status_id')){
            $status = Request::input('status_id');
        }
        $response = \App\Request::getCountByStatus($status);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function show($id)
    {
        $response = \App\Request::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = Request::all();
        $response = \App\Request::create($object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\Request::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function assignRequestToUser()
    {
        $options = Request::all();

        if(!isset($options['request_id']) || !isset($options['user_id']) || !is_numeric($options['user_id']) || !is_numeric($options['request_id'])){
            $response = new \App\Response();
            $response->code = 400;
            $response->msg = "Los datos proporcionados son incorretos";
        } else {
            $response = \App\Request::assignRequestToUser($options['request_id'], $options['user_id']);
        }

        return response()->json($response)->setStatusCode($response->code);
    }

    public function getRequestUser()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $response = \App\RequestUser::getAll($user);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function getByUserCount()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $response = \App\RequestUser::getByUserCount($user->id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function patch($id)
    {
        $data = Request::all();
        $response = \App\Request::patch($id, $data);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function checkRequestStatus($id)
    {
        $status_id = null;
        if(null !== Request::input('status_id')){
            $status_id = Request::input('status_id');
        }
        $response = \App\Request::checkRequestStatus($id, $status_id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function getRequestTypes()
    {
        $response = new \App\Response();
        $response->rows = \App\RequestType::all();
        $response->code = 200;
        return response()->json($response)->setStatusCode($response->code);
    }

    public function NewRequest()
    {
        Event::fire(new NewRequest(['info'=>"Se creo un nuevo request!"]));
    }
}
