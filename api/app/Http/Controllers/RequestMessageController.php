<?php

namespace App\Http\Controllers;

use Request;
use \App\Utils;

class RequestMessageController extends Controller
{

    public function index($id)
    {
        $response = \App\RequestMessage::getAll((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = Request::all();
        $response = \App\RequestMessage::create($object);

        return response()->json($response)->setStatusCode($response->code);

    }

}
