<?php

namespace App\Http\Controllers;


use Request;

class RequestOriginController extends Controller
{
    public function index(){
        $info = false;
        $query = Request::query();
        if(isset($query['info']) && $query['info'] === 'true'){
            $info = true;
        }
        $response = \App\RequestOrigin::getAll($info);

        return response()->json($response)->setStatusCode($response->code);
    }
}
