<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class RutaController extends Controller
{
    public function index()
    {
        $response = \App\Ruta::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = (object)Request::all();
        $object = Utils::cast('\App\Ruta', $object);
        $response = \App\Ruta::createObject($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Ruta::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\Ruta::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Ruta::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }
}
