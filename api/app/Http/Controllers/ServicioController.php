<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServicioController extends Controller
{
    public function index(Request $request)
    {
        $response = \App\Servicio::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }
}
