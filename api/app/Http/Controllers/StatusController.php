<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class StatusController extends Controller
{
    public function index(){
        $response = \App\Status::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }
}
