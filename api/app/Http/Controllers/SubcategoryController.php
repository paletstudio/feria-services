<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class SubcategoryController extends Controller
{

    public function index()
    {
        $info = false;
        $response = \App\Subcategory::getAll();
        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = (object)Request::all();
        $object = Utils::cast('\App\Subcategory', $object);
        $response = \App\Subcategory::createObject($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Subcategory::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\Subcategory::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Subcategory::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }

}
