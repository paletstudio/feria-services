<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;
use JWTAuth;

class UserController extends Controller
{
    public function index()
    {
        $info = false;
        $query = Request::query();
        if(isset($query['info']) && $query['info'] === 'true'){
            $info = true;
        }
        $response = \App\User::getAll($info);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function show($id)
    {
        $response = \App\User::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $object = Request::all();
        //$object = Utils::cast('\App\User', $object);
        $response = \App\User::create($object);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function update($id)
    {
        $object = Request::all();
        $response = \App\User::updateObject($id, $object);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\User::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function changePassword($id)
    {
        //validar que el id y el id del token sean el mismo, sino, 403
        $token = JWTAuth::parseToken();
        $payload = $token->getPayload();
        $user_id = $payload['user']['id'];

        if($user_id == $id) {
            $object = Request::all();
            $response = \App\User::changePassword($id, $object);
        } else {
            $response = new \App\Response(403, 'No tienes permiso para realizar esta acción.');
        }

        return response()->json($response)->setStatusCode($response->code);
    }

    public function login()
    {
        $credentials = Request::only('username', 'password');

        $response = \App\User::login($credentials);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function logout()
    {
        $response = new \App\Response();
        if($token = JWTAuth::getToken()){

            if(JWTAuth::invalidate($token)){
                $response->code = 200;
                $response->msg = "Logout con exito.";
            }
        }
        return response()->json($response)->setStatusCode($response->code);
    }

    public function refreshtoken()
    {
        $response = \App\User::refreshToken();
        return response()->json($response)->setStatusCode($response->code);
    }

    public function auditor()
    {
        $response = \App\User::getAuditors();
        return response()->json($response)->setStatusCode($response->code);
    }

    public function moderator()
    {
        $response = \App\User::getModerators();
        return response()->json($response)->setStatusCode($response->code);
    }

    public function assignEmployees()
    {
        $auditor = Request::input('auditor');
        $employees = Request::input('employees');
        $response = \App\User::assignEmployees($auditor, $employees);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function getEmployees($id)
    {
        $response = \App\User::getEmployees($id);
        return response()->json($response)->setStatusCode($response->code);
    }

    public function getRutas()
    {
        if($token = JWTAuth::parseToken()){
            $payload = $token->getPayload();
            $role = $payload['role'];
            if($role === 'user'){
                $user_id = $payload['user']['id'];
                $response = \App\User::getRutas($user_id);
            } else {
                //403
            }
        } else {
            //400
        }

        return response()->json($response)->setStatusCode($response->code);
    }

}
