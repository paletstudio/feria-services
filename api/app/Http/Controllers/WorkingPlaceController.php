<?php

namespace App\Http\Controllers;

use Request;
use Response;
use \App\Utils;

class WorkingPlaceController extends Controller
{

    public function index()
    {
        $response = \App\Workingplace::getAll();

        return response()->json($response)->setStatusCode($response->code);
    }

    public function create()
    {
        $category = (object)Request::all();
        $category = Utils::cast('\App\Workingplace', $category);
        $response = \App\Workingplace::createObject($category);

        return response()->json($response)->setStatusCode($response->code);

    }

    public function show($id)
    {
        $response = \App\Workingplace::get((int) $id);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function update($id)
    {
        $category = Request::all();
        $response = \App\Workingplace::updateObject($id, $category);

        return response()->json($response)->setStatusCode($response->code);
    }

    public function delete($id)
    {
        $response = \App\Workingplace::deleteObject($id);
        return response()->json($response)->setStatusCode($response->code);
    }

}
