<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [ 'request*', 'user*', 'category*', 'subcategory*', 'generalcatalog/oxxo*', 'workingplace*', 'generalcatalog/ruta*', 'generalcatalog/estacion*', 'generalcatalog/centroemision*'
        //
    ];
}
