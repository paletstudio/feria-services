<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['prefix' => 'api'], function () {
    //Routes without token
    Route::post('login', 'UserController@login');
    Route::post('logout', 'UserController@logout');

    //Requests out dashborad
    Route::group(['prefix' => 'public'], function(){
        Route::post('/request', 'RequestController@create');
        Route::get('/category', 'CategoryController@index');
        Route::get('/subcategory', 'SubcategoryController@index');
        Route::get('/oxxo', 'OxxoController@index');
        Route::get('/ruta', 'RutaController@index');
        Route::get('/centroemision', 'CEController@index');
        Route::get('/estacion', 'EstacionController@index');
        Route::get('/request/new', 'RequestController@NewRequest');
        Route::get('/image/{uniqueid}', 'ImageController@display');

    });

    Route::get('refreshtoken', 'UserController@refreshToken');
    //Routes that need token  //'middleware' => 'jwt.auth'
    Route::group(['middleware' => ['before' => 'jwt.auth']], function () {
        //Route::get('graphic', 'IndicatorController@requestStatus');


        Route::group(['prefix' => 'profile'], function () {
            Route::get('', 'ProfileController@index');
            Route::get('/{id}', 'ProfileController@show');
        });


        Route::group(['prefix' => 'user'], function () {
            Route::get('', 'UserController@index');
            Route::get('/{id}', 'UserController@show')->where('id', '[0-9]+');
            Route::get('/{id}/employee', 'UserController@getEmployees')->where('id', '[0-9]+');
            Route::get('/ruta', 'UserController@getRutas');
            Route::get('/auditor', 'UserController@auditor');
            Route::post('/auditor', 'UserController@assignEmployees');
            Route::get('/moderator', 'UserController@moderator');
            Route::put('/{id}', 'UserController@update');
            Route::put('/{id}/password', 'UserController@changePassword');
            Route::post('', 'UserController@create');
            Route::delete('/{id}', 'UserController@delete');
        });

        Route::group(['prefix' => 'category'], function () {
            Route::get('', 'CategoryController@index');
            Route::get('/route', 'CategoryController@getAllByRoute');
            Route::get('/{id}', 'CategoryController@show');
            Route::post('', 'CategoryController@create');
            Route::put('/{id}', 'CategoryController@update');
            Route::delete('/{id}', 'CategoryController@delete');
        });

        // Route::group(['prefix' => 'generalcatalog'], function () {
        //
            Route::get('servicio', 'ServicioController@index');

            Route::group(['prefix' => 'oxxo'], function(){
                Route::get('', 'OxxoController@index');
                Route::get('/{id}', 'OxxoController@show');
                Route::post('', 'OxxoController@create');
                Route::put('/{id}', 'OxxoController@update');
                Route::delete('/{id}', 'OxxoController@delete');
            });

            Route::group(['prefix' => 'ruta'], function(){
                Route::get('', 'RutaController@index');
                Route::get('/{id}', 'RutaController@show');
                Route::post('', 'RutaController@create');
                Route::put('/{id}', 'RutaController@update');
                Route::delete('/{id}', 'RutaController@delete');
            });

            Route::group(['prefix' => 'estacion'], function(){
                Route::get('', 'EstacionController@index');
                Route::get('/{id}', 'EstacionController@show');
                Route::post('', 'EstacionController@create');
                Route::put('/{id}', 'EstacionController@update');
                Route::delete('/{id}', 'EstacionController@delete');
            });

            Route::group(['prefix' => 'centroemision'], function(){
                Route::get('', 'CEController@index');
                Route::get('/{id}', 'CEController@show');
                Route::post('', 'CEController@create');
                Route::put('/{id}', 'CEController@update');
                Route::delete('/{id}', 'CEController@delete');
            });
        // });

        Route::group(['prefix' => 'subcategory'], function () {
            Route::get('', 'SubcategoryController@index');
            Route::get('/{id}', 'SubcategoryController@show');
            Route::post('', 'SubcategoryController@create');
            Route::put('/{id}', 'SubcategoryController@update');
            Route::delete('/{id}','SubcategoryController@delete');
        });

        Route::group(['prefix' => 'request'], function () {
            Route::get('', 'RequestController@index');
            Route::get('/count', 'RequestController@getCountByStatus');
            Route::get('/{id}', 'RequestController@show')->where('id', '[0-9]+');;
            Route::post('', 'RequestController@create');
            Route::put('/{id}', 'RequestController@update')->where('id', '[0-9]+');;
            Route::delete('/{id}','RequestController@delete')->where('id', '[0-9]+');;
            Route::patch('/{id}', 'RequestController@patch')->where('id', '[0-9]+');;
            Route::get('/{id}/status', 'RequestController@checkRequestStatus');
            Route::post('/assign', 'RequestController@assignRequestToUser');

            Route::get('/{id}/message', 'RequestMessageController@index')->where('id', '[0-9]+');;
            Route::post('/message', 'RequestMessageController@create');

            Route::get('/from/user', 'RequestController@getRequestUser');

            Route::get('/user/count', 'RequestController@getByUserCount');
            Route::get('/type', 'RequestController@getRequestTypes');

        });

        Route::group(['prefix' => 'jobposition'], function () {
            Route::get('', 'JobPositionController@index');
            Route::get('/{id}', 'JobPositionController@show');
            Route::post('', 'JobPositionController@create');
            Route::put('/{id}', 'JobPositionController@update');
            Route::delete('/{id}','JobPositionController@delete');
        });

        Route::group(['prefix' => 'workingplace'], function () {
            Route::get('', 'WorkingPlaceController@index');
            Route::get('/{id}', 'WorkingPlaceController@show');
            Route::post('', 'WorkingPlaceController@create');
            Route::put('/{id}', 'WorkingPlaceController@update');
            Route::delete('/{id}','WorkingPlaceController@delete');
        });

        Route::group(['prefix' => 'report'], function () {
            Route::get('/request','ReportController@request');
            Route::get('/request/excel','ReportController@requestToExcel');
        });

        Route::group(['prefix' => 'status'], function () {
            Route::get('', 'StatusController@index');
        });

        Route::group(['prefix' => 'requestorigin'], function () {
            Route::get('', 'RequestOriginController@index');
        });

        Route::group(['prefix' => 'indicator'], function () {
            Route::get('/requestStatus', 'IndicatorController@requestStatus');
            Route::get('/category', 'IndicatorController@category');
            Route::get('/category/average', 'IndicatorController@categoryAverage');
        });
    });
});
