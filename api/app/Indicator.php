<?php

namespace App;
use DB;

class Indicator
{
    //funcion para obtener
    public static function requestStatus($auditor_id = 0)
    {
        $response = new Response();

        try {
            $data = null;

            $query = DB::select('CALL IndicatorRequestStatus(?)', array($auditor_id));

            if($query){
                $data = new \stdClass;
                $data->data = $query;
                $data->total = 0;

                foreach($query as $q){
                    $data->total += $q->Cantidad;
                }
            }

            $response->rows = $data;
            $response->code = 200;

            if(is_null($data)){
                $response->msg = 'No se encontró información disponible para el indicador de sugerencias';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function category($auditor_id = 0)
    {
        $response = new Response();

        try {
            $data = null;
            $type = request('type', 1);

            $query = DB::select('CALL IndicatorRequestCategory(?, ?)', array($type, $auditor_id));

            if($query){
                $data = new \stdClass;
                $data->data = $query;
                $data->total = 0;

                foreach($query as $q){
                    $data->total += $q->Cantidad;
                }
            }

            $response->rows = $data;
            $response->code = 200;

            if(is_null($data)){
                $response->msg = 'No se encontró información disponible para el indicador de sugerencias-categoría';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function categoryAverage($auditor_id = 0)
    {
        $response = new Response();

        try {
            $data = null;

            $query = DB::select('CALL IndicatorRequestCategoryAverage(?)', array($auditor_id));

            if($query){
                $data = new \stdClass;
                $data->data = $query;
            }

            $response->rows = $data;
            $response->code = 200;

            if(is_null($data)){
                $response->msg = 'No se encontró información disponible para el indicador de sugerencias-categoría';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
        }

        return $response;
    }

}
