<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $table = 'issue';
    protected $hidden = ['created_at', 'updated_at'];

    public function dataaaa()
    {
        return $this->hasManyThrough('App\Data', 'subcategory_issue_data');
    }
}
