<?php

namespace App\Listeners;

use Redis;
use App\Events\DelegateRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDelegateNotification
{
    CONST CHANNEL = 'request.delegate';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DelegateRequest  $event
     * @return void
     */
    public function handle(DelegateRequest $event)
    {
        $obj = new \stdClass;
        $obj->data = new \stdClass;
        $obj->data->request_id = $event->data[1];

        $redis = Redis::connection();
        $redis->publish(self::CHANNEL.'.'.$event->data[0], json_encode($obj));
    }
}
