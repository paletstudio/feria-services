<?php

namespace App\Listeners;

use Redis;
use App\Events\NewRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRequestNotification
{
    CONST CHANNEL = 'request.new';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  NewRequest  $event
     * @return void
     */
    public function handle(NewRequest $event)
    {
        $redis = Redis::connection();
        $redis->publish(self::CHANNEL, json_encode($event));
    }
}
