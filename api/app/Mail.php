<?php

namespace App;


class Mail
{
    /**
     * This function sends a static mail to user to tell his request is being processed.
     * @param  string $to User mail
     * @return void
     */
    public static function sendAttendMail($to){
        $name = env('SMTP_NAME');
        $from = env('SMTP_FROM');
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $message .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $message .= '<head>';
        //Cabeceras del mensaje
        $message .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $message .= '<meta charset="utf-8">';
        $message .= '<title>Email Feria</title>';
        $message .= '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
        $message .= '<style>*{margin: 0;padding: 0;}</style>';
        $message .= '</head>';
        $message .= '<body>';
        //Cuerpo del mensaje
        // Usar una imagen ya hosteada.
        $message .= '<img src="'.resource_path('img/firma.png').'" style="width: 100%; height: 100%; margin-top: 10px">';
        $message .= '</body>';

        $message .= '</html>';
        $mail = new \PHPMailer();
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = env('SMTP_HOST');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = env('SMTP_MAILER');                 // SMTP username
        $mail->Password = env('SMTP_PASS');                           // SMTP password
        $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = env('SMTP_PORT');                                    // TCP port to connect to
        $mail->setFrom($from, $name);            //Who is sending the mail
        $mail->addAddress($to);     // Add a recipient     who is recieving the email
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Su queja/sugerencia ha sido recibida.';
        $mail->Body    = $message;

        if(!$mail->send()) {
            throw new \Exception("Message could not be sent.Mailer Error: " . $mail->ErrorInfo, 1);
        } else {

        }
    }

    /**
     * Send a personalized mail to the user
     * @param  String $to      user email
     * @param  String $message The message to send
     * @param  String $subject The message subject
     * @return void
     */
    public static function sendMail($to, $message, $subject){
        $name = env('SMTP_NAME');
        $from = env('SMTP_FROM');
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = env('SMTP_HOST');
        $mail->SMTPAuth = true;
        $mail->Username = env('SMTP_MAILER');
        $mail->Password = env('SMTP_PASS');
        $mail->SMTPSecure = '';
        $mail->Port = env('SMTP_PORT');
        $mail->setFrom($from, $name);
        $mail->addAddress($to);
        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $message;

        if(!$mail->send()) {
            throw new \Exception("Message could not be sent. Mailer Error: " . $mail->ErrorInfo, 1);
        } else {

        }
    }
}
