<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $name;
    protected $description;

    protected $fillable = array('name', 'description');
    protected $table = 'profile';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('id', $id)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información del perfil.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el perfil.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try {
            $response->rows = self::where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de perfiles.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener los perfiles.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

}
