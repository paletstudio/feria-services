<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;

class Report extends Model
{
    public static function request($auditor_id = -1, $paginate = null)
    {
        try {
            $response = new Response();

            $from = request('from', null);
            $to = request('to', null);
            $type = request('type', 1);

            $conditions = [
                'originId' => request('origin', -1),
                'statusId' => request('status', -1),
                'subcategoryId' => request('subcategory', -1),
                'categoryId' => request('category', -1),
                'auditorId' => $auditor_id,
                'from' => 'null',
                'to' => 'null',
                'userId' => -1,
            ];

            if ($from && $to) {
                $conditions['from'] = $from;
                $conditions['to'] = $to;
            }

            if ($auditor_id > 0 && request('employee', null)) {
                $conditions['userId'] = request('employee', null);
            }

            if ($type == 1) {
                $rows = DB::select('CALL ReportRequest(?, ?, ?, ?, ?, ?, ?, ?, ?)', array(
                    $conditions['statusId'],
                    $conditions['originId'],
                    $conditions['categoryId'],
                    $conditions['subcategoryId'],
                    $conditions['from'],
                    $conditions['to'],
                    $conditions['auditorId'],
                    $conditions['userId'],
                    $type
                ));
            } else {
                $rows = DB::select('CALL InternalReportRequest(?, ?, ?, ?, ?, ?, ?, ?)', array(
                    $conditions['statusId'],
                    $conditions['originId'],
                    $conditions['categoryId'],
                    $conditions['subcategoryId'],
                    $conditions['from'],
                    $conditions['to'],
                    $conditions['auditorId'],
                    $conditions['userId']
                ));
            }

            if (request('paginate') && is_null($paginate)) {

                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                $perPage = request('items', 10);
                $currentPageSearchResults = array_slice($rows, (($currentPage - 1) * $perPage), $perPage);
                $paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults, count($rows), $perPage);

                $response->rows = $paginatedSearchResults;
            } else {
                $response->rows = $rows;
            }
            $response->code = 200;

            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de quejas/sugerencias.';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage().$e->getLine();
        }

        return $response;
    }
}
