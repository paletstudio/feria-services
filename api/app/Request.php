<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Event;
use \App\Events\NewRequest;
use \App\Events\DelegateRequest;
use \App\Mail;

class Request extends Model
{
    protected $guarded = ['id', 'applicant_name', 'applicant_family_name', 'applicant_family_name_2', 'applicant_email', 'applicant_phone', 'applicant_identifier', 'origin', 'image'];
    protected $fillable = ['subcategory_id', 'subject', 'description', 'subcategory_value_1', 'subcategory_value_2', 'status_id', 'response', 'active', 'resolution'];
    protected $table = 'request';

    protected $hidden = [
        'updated_at',
    ];

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function origin()
    {
        return $this->belongsTo('App\RequestOrigin', 'requestorigin_id');
    }

    public function assigned(){
        return $this->hasOne('App\RequestStatus')->where('status_id', 2)->orderBy('id', 'DESC');
    }

    public function resolved(){
        return $this->hasOne('App\RequestStatus')->where('status_id', 3)->orderBy('id', 'DESC');
    }

    public function responded(){
        return $this->hasOne('App\RequestStatus')->where('status_id', 4)->orderBy('id', 'DESC');
    }

    public function image()
    {
        return $this->hasOne('App\RequestImage');
    }

    public function data_value()
    {
        return $this->hasMany('App\RequestDataValue');
    }

    public function issue()
    {
        return $this->belongsTo('App\Issue');
    }

    public function type()
    {
        return $this->belongsTo('App\RequestType', 'request_type_id');
    }

    public static function get($id)
    {
        $response = new Response();

        try {
            $request = self::where([['id', '=', $id], ['active', '=', 1]])
                ->with(['status', 'origin', 'subcategory.category', 'image', 'assigned.user','resolved.user', 'responded.user', 'data_value.data', 'type', 'issue'])
                ->first();

            if ($request) {
                $request->category = $request->subcategory->category;
                unset($request->subcategory->category);
            } else {
                $response->msg = 'No se encontró información de la queja';
            }

            $response->rows = $request;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener la queja.';
            $response->exception = $e->getMessage().$e->getLine();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try {
            $info = request('info', false);
            $type = request('type', 1);
            $paginate = request('paginate', false);
            $status = request('status', null);

            $requests = self::with('status')
                ->where([
                    ['active', '=', 1],
                    ['request_type_id', '=', $type]
                ])
                ->orderBy('id','desc');

            if ($status) $requests->where('status_id', $status);

            if ($info) {
                $requests->with('origin', 'subcategory.category','assigned.user', 'resolved.user', 'responded.user');

                if ($type == 1) {
                    $requests->join('request_data_value as rdv', 'request.id', '=', 'rdv.request_id')
                        ->groupBy('request.id')
                        ->select(
                            '*',
                            DB::raw('MAX(CASE WHEN data_id = 1 THEN value END) AS subject'),
                            DB::raw('MAX(CASE WHEN data_id = 2 THEN value END) AS description')
                        );
                } else {
                    $requests->with('issue');
                }
            }

            $response->rows = $paginate ? $requests->paginate(request('items', 10), null, null, request('page', 1)) : $requests->get();
            $response->code = 200;

            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de quejas';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las quejas.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getCountByStatus($status = 1)
    {
        $response = new Response();

        try{
            $response->rows = self::where([['active','=', 1], ['status_id', '=', $status]])->count();
            $response->code = 200;
        }
        catch (\Exception $e){
            $response->msg = "Se produjo un error al obtener las quejas";
            $response->exception = $e->getMessage();
        }
        return $response;
    }

    public static function create(array $attributes = [])
    {
        $response = new Response();
        DB::beginTransaction();
        try {
            $request = new self();
            $request->applicant_name = $attributes['applicant_name'];
            $request->applicant_family_name = $attributes['applicant_family_name'];
            $request->applicant_family_name_2 = array_key_exists('applicant_family_name_2', $attributes) ? $attributes['applicant_family_name_2'] : null;
            $request->applicant_email = $attributes['applicant_email'];
            $request->applicant_phone = $attributes['applicant_phone'];
            $request->applicant_is_folio = array_key_exists('applicant_is_folio', $attributes) ? $attributes['applicant_is_folio'] : null;
            $request->applicant_identifier = array_key_exists('applicant_identifier', $attributes) ? $attributes['applicant_identifier'] : null;
            $request->subcategory_id = $attributes['subcategory_id'];
            $request->requestorigin_id = $attributes['requestorigin_id'];
            $request->status_id = $attributes['status_id'];
            $request->user_id = $attributes['user_id'];
            $request->issue_id = array_key_exists('issue_id', $attributes) ? $attributes['issue_id'] : null;
            $request->request_type_id = $attributes['request_type_id'];

            $request->save();

            $requestData = [];

            foreach ($attributes['data'] as $key => $data) {
                if ($data['data']['id'] == 5) { //ID=5 es RUTA
                    $rutaId = $data['value']['value_id'];
                }

                $requestDataValue = new RequestDataValue();
                $requestDataValue->data_id = $data['data']['id'];

                //si los valores proporcionados es un arreglo, entonces debe traer value_id
                if (is_array($data['value'])) {
                    $requestDataValue->value = $data['value']['value'];
                    $requestDataValue->value_id = $data['value']['value_id'];

                } else { //Sino, solo se guarda el string
                    $requestDataValue->value = $data['value'];
                }
                array_push($requestData, $requestDataValue);
            }

            $request->data_value()->saveMany($requestData);

            $createStatus = self::createRequestStatus($request->id, $request->status_id, $request->user_id);
            if ($createStatus->code === 200) {

                //We need to verify if any image was sent in order to save it
                if (isset($attributes['image'])) {
                    $file = $attributes['image'];
                    $name = uniqid();
                    $extension = 'jpg';
                    $path = 'images/request/'.$name.'.'.$extension;

                    if (Utils::saveImageBase64($file, public_path($path), $extension)) {
                        $image = new RequestImage();
                        $image->request_id = $request->id;
                        $image->image_url = $path;
                        $image->uniqueid = $name;
                        $image->save();
                    }
                }

                //Codigo para asignar queja a responsable de Ruta
                try {
                    $subcategory = Subcategory::find($request->subcategory_id);
                    if($subcategory) {
                        $subcategory->load('category');
                        if($subcategory->category->use_routes === 1) {

                            $ruta = Ruta::find($rutaId);
                            if($ruta) {
                                self::assignRequestToUser($request->id, $ruta->user_id);
                            }
                        } else if ($subcategory->category->category_type_id == 2) {
                            $subcategoryIssue = SubcategoryIssue::where([
                                ['subcategory_id', '=', $request->subcategory_id],
                                ['issue_id', '=', $request->issue_id]
                            ])->first();

                            if ($subcategoryIssue && !is_null($subcategoryIssue->incharge_user_id)) {
                                self::assignRequestToUser($request->id, $subcategoryIssue->incharge_user_id);
                            }

                        }
                    }
                } catch (\Exception $e) {

                }

                try {
                    $newRequest = self::get($request->id)->rows;
                    Event::fire(new NewRequest($newRequest));
                } catch (\Exception $e) {

                }

                $response->code = 200;
                $response->msg = 'Queja enviada correctamente';
                DB::commit();
                try {
                    Mail::sendAttendMail($request->applicant_email);
                } catch(\Exception $e){

                }
            } else {
                $response->msg = $createStatus->msg;
                $response->exception = $createStatus->exception;
                DB::rollback();
            }
        } catch (\Exception $e) {
            DB::rollback();
            $response->msg = 'Se produjo un error al capturar la queja';
            $response->exception = $e->getMessage().' '.$e->getLine();
            $response->rows = $attributes;
        }

        return $response;
    }

    public static function updateObject($id, $data)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            if ($object) {
                $object->fill($data);
                $object->save();
                if (isset($data['status_id']) && isset($data['user_id'])) {
                    if($data['status_id'] === 3 || $data['status_id'] === 4){
                        self::createRequestStatus($object->id, $data['status_id'], $data['resolve_user_id']);
                    } else {
                        self::createRequestStatus($object->id, $data['status_id'], $data['user_id']);
                    }
                }

                $response->code = 200;
                $response->msg = 'Queja modificada exitosamente';

                //Si el status es igual 3, entonces esta resuelta
                if ($object->status_id === 3) {
                    $response->msg = 'Queja resuelta exitosamente';
                } else if ($object->status_id === 4){
                    $response->msg = 'Queja respondida exitosamente';

                    try {
                        Mail::sendMail($object->applicant_email, $data['response'], $object->subject);
                    }
                    catch (\Exception $e) {

                    }

                }
            } else {
                $response->msg = 'No se encontro informacion con este ID';
            }
        } catch (\Exception $e) {
            $response->msg = 'Hubo un error al modificar la queja';
            $response->exception = $e->getMessage().''.$e->getLine().''.$e->getFile();
        }

        return $response;
    }

    public static function assignRequestToUser($request_id, $user_id)
    {
        $response = new Response();

        try {
            $request = self::find($request_id);
            $user = DB::table('user')->where('id', $user_id)->first();

            if ($request && $user) {
                $table = DB::table('request_user');
                $assign = $table->where('request_id', $request_id)->first();

                if ($assign) {
                    $table->where('request_id', $request_id)->update([
                        'user_id' => $user_id,
                    ]);
                } else {
                    $table->where('request_id', $request_id)->insert([
                        'user_id' => $user_id,
                        'request_id' => $request_id,
                    ]);
                }

                //Change status to assigned
                $request->status_id = 2;
                if($request->save()){
                    self::createRequestStatus($request_id,2,$user_id);
                }

                try {
                    Event::fire(new DelegateRequest([$user_id, $request_id]));
                } catch (\Exception $e) {
                }

                $response->code = 200;
                $response->msg = 'Se ha asignado correctamente la queja.';
            } else {
                $response->code = 400;
                $response->msg = 'Los datos proporcionados son incorretos';
            }
        } catch (\Exception $e) {
            $response->msg = 'Hubo un error al asignar la queja.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createRequestStatus($request_id, $status_id, $user_id = null)
    {
        $response = new Response();

        try {
            $requestStatus = DB::table('request_status');
            $status = $requestStatus->where([['request_id', '=', $request_id], ['status_id', '=', $status_id]])->first();
            //if (!($status)) { //Si no existe el status, creamos el registro
                DB::table('request_status')->insert([
                    'request_id' => $request_id,
                    'status_id' => $status_id,
                    'user_id' => $user_id,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                $response->code = 200;
                $response->rows = true;
                $response->msg = 'Estatus cambiado con exito.';
            /*} else {
                $response->code = 200;
                $response->msg = 'Ya existe este estatus.';
                $response->rows = false;
            }*/
        } catch (\Exception $e) {
            $response->msg = 'Hubo un error al guardar el estatus de la queja.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function patch($id, $data = [])
    {
        $response = new Response();

        try {
            $request = Request::find($id);
            if($request){
                $request->fill($data);
                if($request->save()){
                    $response->code = 200;
                    $response->msg = 'Request actualizada parcialmente.';
                    $response->rows = true;
                }
            } else {
                $response->msg = 'No se encontro informacion con este ID';
            }
        } catch (\Exception $e) {
            $response->msg = 'Hubo un error al actualizar la queja/sugerencia.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function checkRequestStatus($id, $status_id = null)
    {
        $response = new Response();

        try {
            $request = Request::find($id);
            if($request){
                $response->code = 200;
                $response->rows = $request->status_id;
                if($status_id){
                    $response->rows = $request->status_id == $status_id;
                }
            } else {
                $response->msg = 'No se encontro informacion con este ID';
            }
        } catch (\Exception $e) {
            $response->msg = 'Hubo un error al actualizar la queja/sugerencia.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
