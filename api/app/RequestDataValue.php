<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDataValue extends Model
{
    protected $table = 'request_data_value';
    protected $fillable = ['data_id', 'value'];
    public $incrementing = false;
    public $timestamps = false;

    public function data()
    {
        return $this->belongsTo('App\Data');
    }
}
