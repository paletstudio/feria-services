<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestImage extends Model
{
    protected $fillable = ['image_url', 'uniqueid'];
    protected $table = 'requestimage';
    protected $hidden = ['id', 'request_id', 'created_at', 'updated_at', 'image_url'];
}
