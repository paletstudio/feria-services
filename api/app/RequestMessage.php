<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestMessage extends Model
{
    protected $request_id;
    protected $message;
    protected $user_id;

    protected $fillable = array('request_id', 'message', 'user_id');
    protected $table = 'request_message';

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('request_message.id', $id)
                ->join('user', 'user.id', '=', 'request_message.user_id')
                ->select('request_message.id', 'request_message.message', 'user.name', 'user.family_name', 'request_message.created_at')
                ->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de mensajes';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener los mensajes de esta queja.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll($request_id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('request_id', $request_id)
                ->join('user', 'user.id', '=', 'request_message.user_id')
                ->select('request_message.id', 'request_message.message', 'user.name', 'user.family_name', 'request_message.created_at')
                ->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de mensajes';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener los mensajes de esta queja.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function create(array $attributes = [])
    {
        $response = new Response();
        try {
            $object = (object) $attributes;
            $object = Utils::cast('\App\RequestMessage', $object);

            $object->save();
            $message = self::get($object->id)->rows[0];

            $response->rows = $message;
            $response->code = 201;
            $response->msg = 'Mensaje enviado correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear el mensaje';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
