<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestOrigin extends Model
{
    protected $table = 'requestorigin';

    public static function getAll($info = false){
        $response = new Response();

        try {
            $query = self::where('active',1);
            if($info){
                $query->where('visible',1);
            }
            $response->rows = $query->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de origenes.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener los origenes.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
