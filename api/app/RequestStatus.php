<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    protected $table = "request_status";

    public function user(){
        return $this->belongsTo('App\User');
    }
}
