<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RequestUser extends Model
{
    protected $table = 'request_user';
    protected $hidden = [ 'created_at', 'updated_at'];

    public function request()
    {
        return $this->belongsTo('App\Request');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function getAll($user)
    {
        $response = new Response();

        try {
            $type = request('type', 1);
            $ret = [];

            $requests = Request::where([
                ['active', '=', 1],
                ['request_type_id', '=', $type]
            ])
            ->join('request_user as ru', 'request.id', '=', 'ru.request_id')
            ->where('ru.user_id', '=', $user->id)
            ->with('subcategory.category', 'status', 'origin')
            ->groupBy('request.id');

            if ($type == 1) {
                $requests->join('request_data_value as rdv', 'request.id', '=', 'rdv.request_id')
                    ->select(
                        'request.*',
                        DB::raw('MAX(CASE WHEN data_id = 1 THEN value END) AS subject'),
                        DB::raw('MAX(CASE WHEN data_id = 2 THEN value END) AS description')
                    );
            } else {
                $requests->with('issue')->select('request.*');
            }


            if (request('paginate')) {
                $requests = $requests->paginate(request('items', 10), null, null, request('page', 1));

                if($requests->total() === 0){
                    $response->msg = 'No se encontraron quejas asignadas';
                }
            } else {
                $requests = $requests->get();
                if(count($requests) === 0){
                    $response->msg = 'No se encontraron quejas asignadas';
                }
            }

            $response->rows = $requests;
            $response->code = 200;
        } catch (\Exception $e){
            $response->msg = 'Hubo un error al obtener las quejas del usuario.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getByUserCount($user_id)
    {
        $response = new Response();

        try {
            $count = self::where('request_user.user_id', '=', $user_id)
                    ->join('request', function ($join) {
                        $join->on('request_user.request_id', '=', 'request.id')
                            ->where('request.status_id', '=', 2);
                        })
                    ->count();

            $response->rows = $count;
            $response->code = 200;
        } catch (\Exception $e){
            $response->msg = 'Hubo un error al obtener las quejas del usuario.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
