<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model
{
    protected $name;
    protected $description;

    protected $fillable = array('category_id', 'name', 'description', 'user_id');
    protected $table = 'ruta';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::where('id', $id)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de ruta.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el registro de ruta.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try {
            $response->rows = self::where('active', 1)->with('category')->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de rutas';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las rutas.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createObject(Ruta $object)
    {
        $response = new Response();
        try {
            $object->save();
            $response->code = 201;
            $response->msg = 'Ruta creado correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear la ruta';
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

    public static function updateObject($id, $data)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Ruta modificada exitosamente';
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al modificar la ruta';
            $response->exception = $e->getMessage().''.$e->getLine();
        }

        return $response;
    }

    public static function deleteObject($id)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            if ($object) {
                $object->active = 0;
                $object->save();
                $response->msg = 'Ruta eliminada correctamente';
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = 'No se encontro informacion con este id.';
            }

            $response->code = 200;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al eliminar la ruta';
            $response->exception = $e->getMessage();
        }

        return $response;
    }
}
