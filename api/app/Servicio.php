<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicio';
    protected $hidden = ['created_at', 'updated_at'];

    public static function getAll()
    {
        $response = new Response();

        try {
            $rows = Servicio::where('active', 1)->get();

            $response->rows = $rows;
            $response->code = 200;
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
            $response->msg = 'Se produjo un error';
        }

        return $response;
    }
}
