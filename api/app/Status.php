<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    public static function getAll(){
        $response = new Response();

        try {
            $response->rows = self::where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de perfiles.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener los estatus.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

}
