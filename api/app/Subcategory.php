<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $name;
    protected $description;
    protected $category_id;

    protected $fillable = ['name', 'description'];
    protected $table = 'subcategory';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function data()
    {
        return $this->belongsToMany('App\Data', 'subcategory_data')->orderBy('order');
    }

    // public function issues()
    // {
    //     return $this->belongsToMany('App\Issue', 'subcategory_issue')->withPivot('id', 'subcategory_id', 'issue_id');
    // }

    public function issues()
    {
        return $this->hasMany('App\SubcategoryIssue')->with('issue');
    }

    public static function get($id)
    {
        $response = new Response();

        try{
            $response->rows = self::with('category')->where('id', $id)->where('active',1)->get();
            $response->code = 200;
            if(count($response->rows) == 0){
                $response->msg = 'No se encontró información de categorías';
            }
        }
        catch(\Exception $e){
            $response->msg = 'Se produjo un error al obtener la subcategoría.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll($info = false)
    {
        $response = new Response();

        try{
            $info = request('info', false);
            $response->rows = self::where('active',1)->get();
            if($info){
                $response->rows->load('category', 'data');
            }
            $response->code = 200;
            if(count($response->rows) == 0){
                $response->msg = 'No se encontró información de categorías';
            }
        }
        catch(\Exception $e){
            $response->msg = 'Se produjo un error al obtener las categorías.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createObject(Subcategory $object){
        $response = new Response();
        try{
            $object->save();
            $response->code = 201;
            $response->msg = "Subcategoría creada correctamente";
        }
        catch(\Exception $e){
            $response->msg = "Se produjo un error al crear la subcategoría";
            $response->exception = $e->getMessage();
            $response->code = 500;
        }
        return $response;
    }

    public static function updateObject($id, $data){
        $response = new Response();
        try{

            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Subcategoría modificada exitosamente';
        }
        catch(\Exception $e){
            $response->code = 500;
            $response->msg = "Hubo un error al modificar la subcategoría";
            $response->exception = $e->getMessage().''.$e->getLine();
        }
        return $response;
    }

    public static function deleteObject($id){
        $response = new Response();
        try{
            $object = self::find($id);
            if($object){
                $object->active = 0;
                $object->save();
                $response->msg = "Subcategoría borrada correctamente";
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = "No se encontro informacion.";
            }

            $response->code = 200;
        }
        catch(\Exception $e){
            $response->code = 500;
            $response->msg = "Hubo un error al borrar la subcategoría";
            $response->exception = $e->getMessage();
        }
        return $response;
    }
}
