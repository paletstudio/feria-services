<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcategoryIssue extends Model
{
    protected $table = 'subcategory_issue';

    public function issue()
    {
        return $this->belongsTo('App\Issue');
    }

    public function data()
    {
        return $this->hasMany('App\SubcategoryIssueData')->orderBy('order')->with('data');
    }
}
