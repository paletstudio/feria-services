<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcategoryIssueData extends Model
{
    protected $table = 'subcategory_issue_data';

    public function data()
    {
        return $this->belongsTo('App\Data');
    }
}
