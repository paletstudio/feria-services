<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use JWTAuth;
use DB;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password', 'jobposition_id', 'workingplace_id', 'profile_id','requestorigin_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'user';

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    public function jobposition()
    {
        return $this->belongsTo('App\Jobposition');
    }

    public function employees()
    {
        return $this->hasMany('App\BossEmployee', 'boss_id');
    }

    public function rutas()
    {
        return $this->hasMany('App\Ruta');
    }

    public function boss()
    {
        return $this->hasOne('App\BossEmployee', 'employee_id');
    }

    public function workingplace()
    {
        return $this->belongsTo('App\Workingplace');
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = \Hash::make($pass);
    }

    public function requestorigin()
    {
        return $this->belongsTo('App\RequestOrigin');
    }

    public static function getAll($info = false)
    {
        $response = new Response();

        try {
            $rows = self::where('active', 1)->get();
            if ($info) {
                $rows->load('profile', 'jobposition', 'workingplace','requestorigin');
            }
            $response->rows = $rows;
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de usuarios.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener los usuarios.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function get($id)
    {
        $response = new Response();

        try {
            $response->rows = self::with('profile', 'jobposition', 'workingplace')->where('id', $id)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de usuario.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el usuario.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAuditors()
    {
        $response = new Response();

        try {
            $response->rows = self::with('profile', 'employees')
            ->where('profile_id', 3)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de usuario.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el usuario.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getModerators()
    {
        $response = new Response();

        try {
            $response->rows = self::with('profile', 'requestorigin', 'boss')->where('profile_id', 2)->where('active', 1)->get();
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No se encontró información de usuario.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener el usuario.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getRutas($id)
    {
        $response = new Response();

        try {
            $user = self::find($id);
            if($user) {
                $user->load('rutas.category');
                $response->rows = $user->rutas;
            }
            $response->code = 200;
            if (count($response->rows) == 0) {
                $response->msg = 'No hay rutas asignadas para este usuario.';
            }
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al obtener las rutas del usuario.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function create(array $attributes = [])
    {
        $response = new Response();
        try {
            $user = new self();
            $user->name = $attributes['name'];
            $user->family_name = $attributes['family_name'];
            $user->family_name_2 = $attributes['family_name_2'];
            $user->email = $attributes['email'];
            $user->username = $attributes['username'];
            $user->password = $attributes['password'];
            $user->profile_id = $attributes['profile_id'];
            $user->workingplace_id = $attributes['workingplace_id'];
            $user->jobposition_id = $attributes['jobposition_id'];
            $user->requestorigin_id = $attributes['requestorigin_id'];
            $user->save();

            $response->code = 201;
            $response->msg = 'Usuario creado correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear el usuario.';
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

    public static function updateObject($id, $data)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Usuario modificado exitosamente';
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al modificar la categoría';
            $response->exception = $e->getMessage().''.$e->getLine();
        }

        return $response;
    }

    public static function deleteObject($id)
    {
        $response = new Response();
        try {
            $object = self::find($id);
            if ($object) {
                $object->active = 0;
                $object->save();
                $response->msg = 'Usuario eliminado correctamente';
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = 'No se encontro informacion.';
            }

            $response->code = 200;
        } catch (\Exception $e) {
            $response->code = 500;
            $response->msg = 'Hubo un error al eliminar el usuario';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function changePassword($id, $data)
    {
        $response = new Response();

        try {
            $user = User::find($id);
            if ($user) {
                if (\Hash::check($data['oldPassword'], $user->password)) {
                    $response = User::updateObject($id, array('password' => $data['newPassword']));
                    if($response->code == 200) {
                        $response->msg = "Contraseña cambiada exitosamente";
                    }
                } else {
                    $response->rows = false;
                    $response->code = 403;
                    $response->msg = 'La contraseña actual no coincide';
                }
            } else {
                $response->code = 404;
                $response->rows = false;
                $response->msg = 'Usuario no encontrado';
            }
        } catch (\Exception $e) {
            $response->exception = $e->getMessage();
            $response->msg = 'Se produjo un error';
        }

        return $response;
    }

    public static function login($credentials)
    {
        $response = new Response();

        try {
            $user = self::where('username', $credentials['username'])->first();
            if($user && \Hash::check($credentials['password'], $user->password)){
                $user->load('profile','requestorigin');

                $customClaims['user'] = new \stdClass;
                $customClaims['user']->id = $user->id;
                $customClaims['user']->name = $user->name;
                $customClaims['user']->family_name = $user->family_name;
                $customClaims['user']->family_name_2 = $user->family_name_2;
                $customClaims['user']->profile = $user->profile->description;
                $customClaims['user']->email = $user->email;

                $customClaims['user']->requestorigin = null;
                if(!is_null($user->requestorigin)){
                    $customClaims['user']->requestorigin = $user->requestorigin->id;
                }

                $customClaims['role'] = $user->profile->name;

                $response->token = JWTAuth::fromUser($user, $customClaims);
                $response->code = 200;
                $response->msg = 'Login con éxito';
            } else {
                $response->code = 401;
                $response->msg = 'Credenciales invalidas';
            }
        } catch (\Exception $e){
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function refreshToken(){
        $response = new Response();
        $token = JWTAuth::getToken();
        if(!$token){
            $response->code = 400;
            $response->exception = 'Token not provided';
        }
        try{
            $response->rows = JWTAuth::refresh($token);
            $response->code = 200;
        }catch(TokenInvalidException $e){
            $response->code = 500;
            $response->exception = $e;
            // throw new AccessDeniedHttpException('The token is invalid');
        }
        return $response;
    }

    public static function assignEmployees($auditor, array $employees)
    {
        $response = new Response();
        $insert = [];
        try {
            if (count($employees) > 0 && $auditor) {
                DB::table('boss_employee')->where('boss_id', $auditor)->delete();
                foreach($employees as $e) {
                    array_push($insert, ['boss_id' => $auditor, 'employee_id' => $e]);
                }
                DB::table('boss_employee')->insert($insert);
                $response->msg = "Se asignaron los usuarios correctamente";
                $response->code = 200;
            } else {
                $response->code = 400;
                $response->msg = "Se produjo un error";
                $response->exception = "Se envió un arreglo vacío";
            }
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->msg = "Se produjo un error";
            $response->exception = $e->getMessage();
        }
        return $response;
    }

    public static function getEmployees($id)
    {
        $response = new Response;

        try {
            $user = User::find($id);
            if ($user && $user->profile_id == 3) {
                $user->load('employees.user.rutas');
                $response->code = 200;
                $response->rows = $user;
            } else {
                $response->code = 403;
                $response->msg = "El usuario no es un auditor";
            }
        }
        catch (\Exception $e) {
            $response->code = 500;
            $response->msg = "Se produjo un error";
            $response->exception = $e->getMessage();
        }
        return $response;

    }
}
