<?php

namespace App;
use Image;

class Utils
{
    public static function optionalField($field, $defaultValue = null)
    {
        return ((!isset($field) || $field == 'null') ? $defaultValue : $field);
    }
    /**
     * [This function casts any object into an specific class instance]
     * @param  [class, string] $destination  [description]
     * @param  [stdClass] $sourceObject [description]
     * @return [class]               [description]
     */

    protected static $bannedProperties = ['table', 'hidden', 'fillable', 'guarded', 'timestamps'];

    public static function saveImageBase64($file, $path, $extension = 'png')
    {
        try{
            $base64 = $file["base64"];
            $image = Image::make($base64)->encode($extension);

            if(!isset($file["filesize"])){
                $file["filesize"] = $image->filesize();
            }

            $imageWidth = 0;
            if($file['filesize'] > 30000){ //30kb
                $imageWidth = (int)round($image->width() / 2, PHP_ROUND_HALF_DOWN);
                $image->widen($imageWidth);
            } elseif ($file['filesize'] > 2000000){ //2mb
                $imageWidth = (int)round($image->width() /8, PHP_ROUND_HALF_DOWN);
                $image->widen($imageWidth);
            }

            $image->save($path);
        } catch(\Exception $e){
            throw new \Exception($e);
        }

        return true;

    }

    public static function cast($destination, $sourceObject)
    {
        $bannedProperties = self::$bannedProperties;

        if (is_string($destination)) {
            $destination = new $destination();
        }

        $sourceReflection = new \ReflectionObject($sourceObject);
        $destinationReflection = new \ReflectionObject($destination);
        $sourceProperties = $sourceReflection->getProperties();

        foreach ($destinationReflection->getProperties() as $destinationProperty){
            if(get_class($destination) === $destinationProperty->class){
                $name = $destinationProperty->getName();

                if(!in_array($name, $bannedProperties)){
                    $destination->$name = null;
                }
            }
        }

        foreach ($sourceProperties as $sourceProperty) {
            $sourceProperty->setAccessible(true);
            $name = $sourceProperty->getName();
            $value = $sourceProperty->getValue($sourceObject);

            if ($destinationReflection->hasProperty($name)) {
                $destination->$name = $value;
            }
        }

        return $destination;
    }

    public static function filterExcelValues($row)
    {
        $undesiredProperties = ['applicant_is_folio', 'subcategory_id', 'issue_id', 'request_origin_id', 'status_id', 'user_id', 'updated_at', 'resolution', 'request_type_id', 'requestorigin_id'];
        $translations = array(
            'request_type' => 'Tipo de sugerencia',
            'applicant_name' => 'Nombre',
            'applicant_family_name' => 'Apellido Paterno',
            'applicant_family_name_2' => 'Apellido Materno',
            'applicant_phone' => 'Teléfono',
            'applicant_email' => 'Email',
            'applicant_identifier' => 'Folio/Tarjeta',
            'active' => 'Activo',
            'created_at' => 'Fecha de creación',
            'status_name' => 'Estatus',
            'category_name' => 'Categoria',
            'subcategory_name' => 'Subcategoria',
            'issue_name' => 'Problema',
            'origin_name' => 'Origen',
            'response' => 'Respuesta',
            'response_datetime' => 'Fecha de respuesta',
            'response_time' => 'Tiempo de respuesta (en minutos)',
        );

        foreach ($undesiredProperties as $property) {
            if (property_exists($row, $property)) unset($row->{$property});
        }

        $oldRow = $row;
        $row = new \stdClass;
        $row->id = $oldRow->id;
        unset($oldRow->id);

        foreach ($translations as $key => $newKey) {
            if (property_exists($oldRow, $key)) {
                $row->{$newKey} = $oldRow->{$key};
                unset($oldRow->{$key});
            }
        }

        $ret = (object) array_merge((array) $row, (array) $oldRow);

        return $ret;
    }
}
