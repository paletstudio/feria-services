<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workingplace extends Model
{
    protected $name;
    protected $description;

    protected $fillable = array('name', 'description');
    protected $table = 'workingplace';

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function get($id)
    {
        $response = new Response();

        try{
            $response->rows = self::where('id', $id)->where('active',1)->get();
            $response->code = 200;
            if(count($response->rows) == 0){
                $response->msg = 'No se encontró información de lugares de trabajo';
            }
        }
        catch(\Exception $e){
            $response->msg = 'Se produjo un error al obtener la información.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function getAll()
    {
        $response = new Response();

        try{
            $response->rows = self::where('active',1)->get();
            $response->code = 200;
            if(count($response->rows) == 0){
                $response->msg = 'No se encontró información de lugares de trabajo.';
            }
        }
        catch(\Exception $e){
            $response->msg = 'Se produjo un error al obtener la información.';
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function createObject(Workingplace $object){
        $response = new Response();
        try{
            $object->save();
            $response->code = 201;
            $response->msg = "Lugar de trabajo creada correctamente.";
        }
        catch(\Exception $e){
            $response->msg = "Se produjo un error al crear el lugar de trabajo.";
            $response->exception = $e->getMessage();
            $response->code = 500;
        }
        return $response;
    }

    public static function updateObject($id, $data){
        $response = new Response();
        try{

            $object = self::find($id);
            $object->fill($data);
            $object->save();

            $response->code = 200;
            $response->msg = 'Lugar de trabajo modificado exitosamente';
        }
        catch(\Exception $e){
            $response->code = 500;
            $response->msg = "Hubo un error al modificar el lugar de trabajo";
            $response->exception = $e->getMessage().''.$e->getLine();
        }
        return $response;
    }

    public static function deleteObject($id){
        $response = new Response();
        try{
            $object = self::find($id);
            if($object){
                $object->active = 0;
                $object->save();
                $response->msg = "Lugar de trabajo borrado correctamente";
                $response->rows = true;
            } else {
                $response->rows = false;
                $response->msg = "No se encontro información.";
            }

            $response->code = 200;
        }
        catch(\Exception $e){
            $response->code = 500;
            $response->msg = "Hubo un error al borrar el lugar de trabajo";
            $response->exception = $e->getMessage();
        }
        return $response;
    }

}
