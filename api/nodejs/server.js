var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('request.new', function(err, count) {});
redis.psubscribe('request.delegate.?', function(err, count) {});

redis.on('message', function(channel, message) {
    //console.log('Mensaje recibido: ' + message);
    message = JSON.parse(message);
    setTimeout(function() {
        io.emit(channel, message.data);
    }, 1000);
});

redis.on('pmessage', function(subscribed, channel, message) {
    console.log('Nueva queja delegada a ' + new Date());
    message = JSON.parse(message);
    setTimeout(function() {
        io.emit(channel, message.data);
    }, 1000);
});

http.listen(3000, function() {
    console.log('Listening on Port 3000');
});
